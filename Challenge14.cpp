#include<iostream>
#include<algorithm>
using namespace std;

void quicksort (int arr[],int start,int end);
void mergesort (int arr[],int low,int high);                                                    //Declairing functions.
void merge(int array[], int low, int middle, int high);


int main()
{
	int arr[10] = {9,7,6,1,2,4,8,5,3,10};                                                                 //Set the array.
	cout << "Before apply Quick Sort : " << endl;
	for(int i=0 ; i<10 ; i++) 
	{
		cout << arr[i] << " ";
	}
	cout << endl<<endl<<"Quick Sorted : "<<endl;
	
	quicksort(arr,0,9);
	for(int i=0 ; i<10 ; i++) 
	{
		cout << arr[i] << " ";
	}

	
}

void quicksort (int arr[],int start,int end)                                                          // The Quick sort function.
{
	int hold_start = start ,hold_end = end , mid = (start+end)/2 +1 , hold[3] ,pivot;                 //Calculate/ Pivot value.
	hold[0] = arr[start];
	hold[1] = arr[mid];
	hold[2] = arr[end];
	
	for(int  i=0; i<2; i++)                                                                          //Sort the first,middle and last.
	{
		int min = i;
		for(int j=i+1; j < 3 ; j++)
		{
			if(hold[j] < hold[min])
			{
				swap(hold[i],hold[j]);
			}
		}
	}
	
	arr[start] = hold[0];
	arr[mid] = hold[1];
	arr[end] = hold[2];
	pivot = hold[1];                                                                               //Set pivot value.
	
	while(hold_start <= hold_end)                                                                  //Sort lower element to the left and higher to the right.
	{
		while(arr[hold_start] < pivot)
		{
			hold_start++;
		}
		
		while(arr[hold_end] > pivot)
		{
			hold_end--;
		}
			if(hold_start <= hold_end)
			{
				swap(arr[hold_start],arr[hold_end]);
				hold_start++;
				hold_end--;
			}
				
	}
	
	if(start < hold_end)                                                                             //Sort left side until the size is less than 2
	{
		quicksort(arr,start,hold_end);
	}
	
	if(end > hold_start)                                                                            //Sort right side until the size is less than 2
	{
		quicksort(arr,hold_start,end);
	}
}



void mergesort(int arr[], int low, int high)                                                        //The Merge Sort function.
{

    int middle;
    if(low < high)
    {
   		middle = (low + high) / 2;
    	mergesort(arr, low, middle);
    	mergesort(arr, middle + 1, high);
    	merge(arr, low, middle, high);
    }
}

 void merge(int arr[], int low, int middle, int high)                                            //The Merge function to work with Merge Sort.
{

    int n1 = middle - low + 1;
    int n2 = high - middle;
    int leftArray[n1 + 1];
    int rightArray[n2 + 1];
    for(int i = 1; i <= n1; i++)
    {
        leftArray[i] = arr[low + i - 1];
    }
    for(int j = 1; j <= n2; j++)
	{
        rightArray[j] = arr[middle + j];
    }
    leftArray[n1 + 1] ;
    rightArray[n2 + 1] ;
    int i = 1, j = 1;
    for(int k = low; k <= high; k++)
    {
    	if(leftArray[i] <= rightArray[j])
        {
            arr[k] = leftArray[i];
            i++;
        }
        else
        {
            arr[k] = rightArray[j];
            j++;
        }
    }
}
